package com.kul.films.repository;

import com.kul.films.model.Film;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;


@Getter
@RequiredArgsConstructor
public class FilmsRepository {
    private final List<Film> films;
}
