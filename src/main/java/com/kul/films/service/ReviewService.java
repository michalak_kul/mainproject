package com.kul.films.service;

import com.kul.films.model.Review;
import com.kul.films.repository.ReviewRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ReviewService {
    private final ReviewRepository reviewRepository;

    public List<Review> getReviewsForFilm(String filmName) {
        return reviewRepository
                .getReviews()
                .stream()
                .filter(review -> review
                        .getFilmName()
                        .equals(filmName))
                .collect(Collectors.toList());
    }

    public double calculateAverageRating(String filmName){
        List<Review> reviewsForFilm = getReviewsForFilm(filmName);
        if (reviewsForFilm.isEmpty()){
            return 0d;
        }
        double ratings = reviewsForFilm
                .stream()
                .map(Review::getRating)
                .reduce(0, Integer::sum);
        return ratings/reviewsForFilm.size();
    }

    public boolean addReview(int rating, String email, String filmName) {
        if (checkIfRatingIsAdded(email, filmName)) {
            reviewRepository.addReview(new Review(rating, email, filmName));
            return true;
        }
        return false;
    }

    private boolean checkIfRatingIsAdded(String email, String filmName) {
        return getReviewsForFilm(filmName)
                .stream()
                .noneMatch(review -> review
                        .getEmail()
                        .equals(email));
    }
}
