package com.kul.films.service;

import com.kul.films.model.Film;
import com.kul.films.repository.FilmsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FilmsService {
    private final FilmsRepository filmsRepository;

    public List<Film> getAllFilms(){
        return filmsRepository.getFilms();
    }
}
