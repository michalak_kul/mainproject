package com.kul.films.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class Film {
    private final String name;
    private final String director;
    private final String releaseYear;
    private final String company;
    private final String screenWriter;
}
