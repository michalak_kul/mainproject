package com.kul.films;

import com.kul.films.printers.MainPrinter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class FilmsApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(FilmsApplication.class, args);
        MainPrinter bean = run.getBean(MainPrinter.class);
        bean.process();
    }

}
