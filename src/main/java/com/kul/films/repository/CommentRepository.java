package com.kul.films.repository;

import com.kul.films.model.Comment;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Getter
@RequiredArgsConstructor
public class CommentRepository {
    private final List<Comment> comments;

    public void saveComment(Comment comment) {
        comments.add(comment);
    }
}
