package com.kul.films.repository;

import com.kul.films.model.Review;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Getter
@RequiredArgsConstructor
public class ReviewRepository {
    private final List<Review> reviews;

    public void addReview(Review review) {
        reviews.add(review);
    }
}
