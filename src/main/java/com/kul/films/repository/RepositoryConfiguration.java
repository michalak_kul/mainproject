package com.kul.films.repository;

import com.kul.films.model.Comment;
import com.kul.films.model.Film;
import com.kul.films.model.Review;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class RepositoryConfiguration {

    @Bean
    ReviewRepository reviewRepository(){
        return new ReviewRepository(createStaticReviews());
    }

    @Bean
    CommentRepository commentRepository(){
        return new CommentRepository(createStaticComments());
    }

    @Bean
    FilmsRepository filmsRepository(){
        return new FilmsRepository(createStaticFilms());
    }

    private List<Review> createStaticReviews() {
        return new ArrayList<>(List.of(new Review(5, "jan.testowy@test.pl", "Surykatki w ogniu")));
    }

    private List<Film> createStaticFilms() {
        return new ArrayList<>(List.of(new Film("Surykatki w ogniu", "Chang Lee", "2012", "WarnerBros", "Edwin McGall"),
                new Film("Surykatki w ogniu 2: Zemsta surykatek", "Change Ming", "2013", "Columbia Pictures",
                        "Edwin McGall Jr")));
    }

    private List<Comment> createStaticComments() {
        return new ArrayList<>(List.of(new Comment("jan.testowy@test.pl", "FILM MEGA", "Surykatki w ogniu")));
    }
}
