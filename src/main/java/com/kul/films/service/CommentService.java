package com.kul.films.service;

import com.kul.films.model.Comment;
import com.kul.films.repository.CommentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CommentService {
    private final CommentRepository commentRepository;

    public List<Comment> getAllCommentsForFilm(String filmName) {
        return commentRepository
                .getComments()
                .stream()
                .filter(comment -> comment
                        .getFilmName()
                        .equals(filmName))
                .collect(Collectors.toList());
    }

    public void addComment(String comment, String email, String filmName){
        commentRepository.saveComment(new Comment(email, comment, filmName));
    }
}
