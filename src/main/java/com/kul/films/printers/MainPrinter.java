package com.kul.films.printers;

import com.kul.films.model.Film;
import com.kul.films.service.CommentService;
import com.kul.films.service.FilmsService;
import com.kul.films.service.ReviewService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class MainPrinter {

    private final Scanner scanner = new Scanner(System.in);
    private final FilmsService filmsService;
    private final CommentService commentService;
    private final ReviewService reviewService;

    public void process() {
        System.out.println("Witamy w repozytorium filmów");
        int input;
        do {
            printMenu();
            input = processUserInput();
        } while (input != 4);
    }

    private int processUserInput() {
        int input;
        input = Integer.parseInt(scanner.nextLine());
        switch (input) {
            case 1:
                printFilms();
                processDetails();
                break;
            case 2:
                processCommentOption();
                break;
            case 3:
                processReviewOption();
                break;
            case 4:
                break;
            default:
                System.out.println("Sorry you picked wrong option!");
        }
        return input;
    }

    private void processDetails() {
        System.out.println("Chcesz poznac szczegoly któregoś z filmów? (Podaj nazwę bądź N)");
        String input = scanner.nextLine();
        if(!input.equals("N")){
            printDetails(input);
        }
    }


    private void processReviewOption() {
        System.out.println("Do którego filmu chcesz dodać opinię?");
        String filmName = scanner.nextLine();
        System.out.print("Podaj opinie: ");
        String review = scanner.nextLine();
        System.out.print("Podaj email: ");
        String email = scanner.nextLine();
        if (!reviewService.addReview(Integer.parseInt(review), email, filmName)) {
            System.out.println("Już dodałeś opinię do tego filmu");
        }
    }

    private void processCommentOption() {
        System.out.println("Do którego filmu chcesz dodać komentarz?");
        printFilms();
        String filmName = scanner.nextLine();
        System.out.print("Podaj komentarz: ");
        String comment = scanner.nextLine();
        System.out.print("Podaj email: ");
        String email = scanner.nextLine();
        commentService.addComment(comment, email, filmName);
    }

    private void printFilms() {
        List<String> films = filmsService
                .getAllFilms()
                .stream()
                .map(Film::toString)
                .collect(Collectors.toList());

        for (int i = 0; i < films.size(); i++) {
            System.out.println((i + 1) + ". " + films.get(i));
        }
    }

    private void printDetails(String input) {
        System.out.println("Średnia ocen tego filmu wynosi: " + reviewService.calculateAverageRating(input));
        System.out.println("Komentarze: ");
        commentService.getAllCommentsForFilm(input).forEach(System.out::println);
    }

    private void printMenu() {
        System.out.println("Co chcesz zrobić? ");
        System.out.println("1. Wyświetl filmy");
        System.out.println("2. Dodaj komentarz");
        System.out.println("3. Dodaj ocenę");
        System.out.println("4. Wyjdź");
    }
}
